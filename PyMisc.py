# coding:UTF-8

import imaplib, codecs, sys, codecs

#sys.stdout = codecs.getwriter('shift_jis')(sys.stdout)

g_font_ookisa = 12

def read_NAMEandPASSWORD():
	with codecs.open( "PyTegami.txt", 'r', 'utf-8' ) as f_tmp:
		s_tmp = f_tmp.read()
		sl_tmp = s_tmp.split('\r\n')
		kazu = 0
		username = password = ''
		for gyou in sl_tmp:
			if gyou[:2] == 'ID':
				username = gyou[2:]
				kazu += 1
			elif gyou[:8] == 'PASSWORD':
				password = gyou[8:]
				kazu += 2
			if kazu >= 2:	break
	return ( username, password )

def namae_yomu():
	with codecs.open( "PyTegami.txt", 'r', 'utf-8' ) as f_tmp:
		s_tmp = f_tmp.read()
		tukaite_namae = ''
		for gyou in s_tmp.split('\r\n'):
			if gyou[:2] == 'ID':
				tukaite_namae = gyou[2:]
				break
	return tukaite_namae

def tegami_namae_kaesu():
	with codecs.open( "PyTegami.txt", 'r', 'utf-8' ) as f_kari:
		gyou = 'a'
		while gyou != '':
			gyou = f_kari.readline()
			if gyou[:5] == 'NAMAE':
				return gyou[5:].strip('\r\n')

def ConnectToServer(verbose=False):
	# Connect to the server
	hostname = "imap.gmail.com"
	#print hostname, u'につなげようとしている'
	connection = imaplib.IMAP4_SSL(hostname)

	# Login to our account
	username ,password = read_NAMEandPASSWORD()
	
	#print username, u'としてログインしようとしている'
	connection.login(username, password)
	
	return connection

class AtesakiChou:
	def __init__( self ):
		self.AtesakiTati = []
		self.NamaeTati = []
		try:
			with codecs.open( 'Atesaki.txt', encoding = 'utf-8' ) as f_ate:
				line = f_ate.readline()
				if len( line ) > 0 and line[0] == u'\ufeff':
					line = line[1:]
				if len( line ) > 0:
					if line.find( u'<' ) != -1:
						if line[-2:] == u'\r\n':
							line = line[:-2]
						elif line[-1:] == u'\n' or line[-1:] == u'\r':
							line = line[:-1]
						self.AtesakiTati.append( line )
						namae_nagasa = line.find( u'<' )
						if namae_nagasa != -1:
							self.NamaeTati.append( line[ :namae_nagasa ] )
					for line in f_ate:
						if line[-2:] == u'\r\n':
							line = line[:-2]
						elif line[-1:] == u'\n' or line[-1:] == u'\r':
							line = line[:-1]
						namae_nagasa = line.find( u'<' )
						if namae_nagasa != -1:
							self.NamaeTati.append( line[ :namae_nagasa ] )
							self.AtesakiTati.append( line )
		except:
			pass
		
		self.Kawatta = False
	
	def Subete( self ):
		return tuple( self.AtesakiTati )
	
	def NamaeSubete( self ):
		return tuple( self.NamaeTati )
	
	def Kuwaeru( self, ate ):
		self.AtesakiTati.append( ate )
		self.Kawatta = True
	
	def Nozoku( self, ate ):
		try:
			self.AtesakiTati.remove( ate )
			self.Kawatta = True
		except:
				#print u'宛て先帳にそれは無い：',err
				#print 'Kawatta:', self.Kawatta
				pass
	
	def Kaku( self ):
		if not self.Kawatta:	return
		
		with codecs.open( 'Atesaki.txt', 'r+', encoding = 'utf-8' ) as f_ate:
			for index in range( 0, len( self.AtesakiTati ) ):
				f_ate.write( self.AtesakiTati[ index ] + '\r\n' )
		
		self.Kawatta = False
