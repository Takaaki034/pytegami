# coding:UTF-8
# デバッグ用の窓

import wx

class ItaOmo( wx.Panel ):
	def __init__( self, parent ):
		wx.Panel.__init__( self, parent )
		
		self.parent = parent
		btn_Kesu = wx.Button( self, -1, "消す" )
		self.tc_Log = wx.TextCtrl(self, -1, "", style=wx.TE_READONLY | wx.TE_MULTILINE)

		self.hSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.hSizer.Add( btn_Kesu, 0, wx.SHAPED )
		self.vSizer = wx.BoxSizer( wx.VERTICAL )
		self.vSizer.Add( self.hSizer, 0, wx.SHAPED )
		self.vSizer.Add( self.tc_Log, 1, wx.EXPAND )

		self.Bind( wx.EVT_BUTTON, self.BtnKesuDe, btn_Kesu )

		self.SetSizer(self.vSizer)
		self.SetAutoLayout(1)
		#self.Sizer.Fit(self)

	def BtnKesuDe( self, event ):
		self.tc_Log.Clear()

class DebugMado( wx.Frame ):
	def __init__( self, parent, title = 'デバッグ窓' ):
		wx.Frame.__init__( self, parent, title = title, size = ( 600, 400 ) )

		self.CreateStatusBar()
		file_sinagaki = wx.Menu()
		sina_nukeru = file_sinagaki.Append( wx.ID_EXIT, '抜ける(&N)', 'このソフトを抜ける' )

		self.io = ItaOmo( self )

	def DasiOku( self, humi ):
		if isinstance(humi, str):
			self.io.tc_Log.SetInsertionPoint( 0 )
			self.io.tc_Log.WriteText( humi + '\n' )
