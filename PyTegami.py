# coding:UTF-8
'''電子手紙ソフトウェアー'''

import wx, smtplib, ctypes, sys, codecs, os, re, imap4_utf7, time, imaplib
import email, logging
#import email.Utils
import email.mime.text
#from email.Header import Header
#from email import Header
from PyMisc import ConnectToServer
from PyMisc import g_font_ookisa
import PyOkuru
from debugmado import DebugMado
import email.contentmanager

# グローバル変数
g_tunagu = True
g_imap4 = None
g_debug = False
g_dm = None

# 定数
SIMESU_KUDARI_KAZU = 30

def ServerTunagu():
	global g_imap4
	try:
		g_imap4 = ConnectToServer()
	except:
		wx.MessageBox("サーバーにつなげられませんでした。アカウント設定から名前と合い言葉を入れ直して下さい。", "過ち", wx.OK)
		if g_debug:
			g_dm.DasiOku('ServerTunagu, g_imap4:' + repr(g_imap4))

class KeyEventListBox( wx.ListBox ):

	def __init__( self, parent, id, choices, kansuu ):
		wx.ListBox.__init__( self, parent, id, choices = choices )
		self.Bind( wx.EVT_KEY_UP, self.OnKeyUp )
		self.parent = parent
		self.kansuu = kansuu
	
	def OnKeyUp( self, event ):
		if event.GetKeyCode() == wx.WXK_RETURN:
			pass
			self.kansuu( self.GetSelection() )
		else:	event.Skip()

class KeyEventListCtrl( wx.ListCtrl ):
	def __init__( self, parent ):
		wx.ListCtrl.__init__( self, parent, style = wx.LC_REPORT )
		self.Bind( wx.EVT_KEY_UP, self.OnKeyUp )
		self.parent = parent
	
	def OnKeyUp( self, event ):
		if event.GetKeyCode() == wx.WXK_RETURN:
			if g_imap4 != None:
				pass
				self.parent.HumiwoEru()
		else:	event.Skip()

class MoziSetteiDialog( wx.Dialog ):
	def __init__( self, parent ):
		wx.Dialog.__init__( self, parent, title = '設定', size = ( 530, 320 ) )
		
		self.parent = parent
		
		st_HMOokisa = wx.StaticText( self, label = "文の文字の大きさ" )
		self.tc_HMOokisa = wx.TextCtrl( self )
		btn_Settei = wx.Button( self, -1, "設定" )
		
		HSizer = wx.BoxSizer( wx.HORIZONTAL )
		HSizer.Add( st_HMOokisa, 0, wx.SHAPED )
		HSizer.Add( self.tc_HMOokisa, 0, wx.SHAPED )
		VSizer = wx.BoxSizer( wx.VERTICAL )
		VSizer.Add( HSizer, 0, wx.EXPAND )
		VSizer.Add( btn_Settei )
		
		self.SetSizer( VSizer )
		self.SetAutoLayout( True )
		self.Bind( wx.EVT_BUTTON, self.OnBtnSettei, btn_Settei )
		
	def OnBtnSettei( self, event ):
		global g_font_ookisa
		try:
			g_font_ookisa = int(self.tc_HMOokisa.GetValue())
			font_atarasii = wx.Font(g_font_ookisa, wx.DEFAULT, wx.NORMAL, wx.NORMAL )
			self.parent.tc_Humi.SetFont( font_atarasii )
			self.Close()
		except:
			wx.MessageBox('数を入れ置いて下さい', '過ち', wx.OK | wx.ICON_ERROR )

class OmonaIta(wx.Panel): #このソフトウェアーの主な窓
	def __init__(self, parent):
		self.parent = parent
		wx.Panel.__init__(self, parent)
		
		btn_Hurui = wx.Button( self, -1, "古い" )
		btn_Atarasii = wx.Button( self, -1, "新しい" )
		btn_Kaku = wx.Button( self, -1, "手紙を書く" )
		btn_Kotae = wx.Button( self, -1, "答えを書く" )
		self.cb_Inyou = wx.CheckBox( self, -1, "文を引用(&I)" )
		btn_Kesu = wx.Button( self, -1, "手紙を消す" )
		btn_TBTukuru = wx.Button( self, -1, "手紙箱を作る" )
		btn_TBKesu = wx.Button( self, -1, "手紙箱を消す" )
		btn_MoziSettei = wx.Button( self, -1, "文字の設定" )
		
		lbl_Hako = wx.StaticText( self, label = "箱(A)" )
		self.lb_Folder = KeyEventListBox( self, -1, choices=('1','2','3'), kansuu = self.HakoKaeru )
		lbl_Soe = wx.StaticText( self, label = "添え物")
		self.lb_Soe = KeyEventListBox( self, -1, choices=('1','1','3'), kansuu = self.SoemonoKaku)
		lbl_Kudari = wx.StaticText( self, label = "件(K)" )
		self.lc_Kudari = KeyEventListCtrl( self ) #wx.ListCtrl( self, -1, style = wx.LC_REPORT )
		self.lc_Kudari.InsertColumn( 0, '旗', width = 140 )
		self.lc_Kudari.InsertColumn( 1, '件', width = 130)
		self.lc_Kudari.InsertColumn( 2, '送り主', width = 150 )
		lbl_Humi = wx.StaticText( self, label = "文(H)" )
		self.tc_Humi = wx.TextCtrl(self, -1, "", style=wx.TE_READONLY | wx.TE_MULTILINE)
		
		font = wx.Font( g_font_ookisa, wx.DEFAULT, wx.NORMAL, wx.NORMAL )
		#font_ookii = wx.Font( g_font_ookisa + 2, wx.DEFAULT, wx.NORMAL, wx.NORMAL )
		self.lb_Soe.SetFont(font)
		self.lb_Folder.SetFont( font )
		self.lc_Kudari.SetFont( font )
		self.tc_Humi.SetFont( font )

		self.Bind( wx.EVT_BUTTON, self.OnButtonHurui, btn_Hurui )
		self.Bind( wx.EVT_BUTTON, self.OnButtonAtarasii, btn_Atarasii)
		self.Bind( wx.EVT_BUTTON, self.OnButtonKaku, btn_Kaku )
		self.Bind( wx.EVT_BUTTON, self.OnButtonKotae, btn_Kotae )
		self.Bind( wx.EVT_BUTTON, self.OnBtnKesu, btn_Kesu )
		self.Bind( wx.EVT_BUTTON, self.OnBtnTBTukuru, btn_TBTukuru )
		self.Bind( wx.EVT_BUTTON, self.OnBtnTBKesu, btn_TBKesu )
		self.Bind( wx.EVT_BUTTON, self.OnBtnMoziSettei, btn_MoziSettei )
		self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, lambda x: self.HumiwoEru(), self.lc_Kudari)
		
		self.hSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.hSizer.Add(btn_Hurui, 0, wx.SHAPED )
		self.hSizer.Add(btn_Atarasii, 0, wx.SHAPED )
		self.hSizer.Add(btn_Kaku, 0, wx.SHAPED )
		self.hSizer.Add( btn_Kotae, 0, wx.SHAPED )
		self.hSizer.Add( self.cb_Inyou, 0, wx.SHAPED )
		self.hSizer.Add( btn_Kesu, 0, wx.SHAPED )
		self.hSizer.Add( btn_TBTukuru, 0, wx.SHAPED )
		self.hSizer.Add( btn_TBKesu, 0, wx.SHAPED )
		self.hSizer.Add( btn_MoziSettei, 0, wx.SHAPED )
		
		hSizer2 = wx.BoxSizer(wx.HORIZONTAL)
		hSizer2.Add(self.lb_Folder, 1, wx.EXPAND)
		hSizer2.Add(self.lb_Soe, 1, wx.EXPAND)
		
		hSizer3 = wx.BoxSizer(wx.HORIZONTAL)
		hSizer3.Add(lbl_Hako, 1, wx.EXPAND)
		hSizer3.Add(lbl_Soe, 1, wx.EXPAND)

		self.vSizer = wx.BoxSizer(wx.VERTICAL)
		self.vSizer.Add(self.hSizer, 0, wx.EXPAND)
		#self.hSizer2 = wx.BoxSizer(wx.HORIZONTAL)
		self.vSizer.Add( hSizer3, 0, wx.EXPAND )
		self.vSizer.Add( hSizer2, 1, wx.EXPAND )
		self.vSizer.Add( lbl_Kudari, 0, wx.EXPAND )
		self.vSizer.Add( self.lc_Kudari, 2, wx.EXPAND )
		self.vSizer.Add( lbl_Humi, 0, wx.EXPAND )
		self.vSizer.Add( self.tc_Humi, 4, wx.EXPAND)

		self.SetSizer(self.vSizer)
		self.SetAutoLayout(1)
		self.Sizer.Fit(self)
		
		self.UID_dic = {}
		self.okuruWaku = None
		self.mail_count = 0
		self.imano_page = 0
		self.imano_tegami = None
		self.ImanoHako = 'INBOX'
		self.SoemonoTati = None
		
		self.sc_HumiId = wx.NewId()
		self.sc_KudariId = wx.NewId()
		self.sc_HakoId = wx.NewId()
		self.sc_TKesuId = wx.NewId()
		self.sc_TKotaeId = wx.NewId()
		self.sc_AtarasiiId = wx.NewId()
		self.sc_HuruiId = wx.NewId()
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_HumiId )
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_KudariId )
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_HakoId )
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_TKesuId )
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_TKotaeId )
		self.Bind(wx.EVT_MENU, self.OnKeyCombo, id = self.sc_AtarasiiId )
		self.Bind(wx.EVT_MENU, self.OnKeyCombo, id = self.sc_HuruiId )
		
		accel_tbl = wx.AcceleratorTable( [ ( wx.ACCEL_ALT, ord('H'), self.sc_HumiId ), ( wx.ACCEL_ALT, ord( 'K' ), self.sc_KudariId ), ( wx.ACCEL_ALT, ord( 'A' ), self.sc_HakoId ), ( wx.ACCEL_ALT, ord( 'T' ), self.sc_TKesuId ), (wx.ACCEL_ALT, ord('O'), self.sc_TKotaeId), (wx.ACCEL_ALT, ord('Z'), self.sc_AtarasiiId), (wx.ACCEL_ALT, ord('X'), self.sc_HuruiId) ] )
		self.SetAcceleratorTable( accel_tbl )
		
		if g_imap4 == None:	return
		
		self.HakoItiran()
		
		self.HakoKaeru( 0 ) #KudariToriyose()
	
	def OnKeyCombo( self, event ):
		if event.GetId() == self.sc_HumiId:
			self.tc_Humi.SetFocus()
		elif event.GetId() == self.sc_KudariId:
			#self.lb_Kudari.SetFocus()
			self.lc_Kudari.SetFocus()
		elif event.GetId() == self.sc_HakoId:
			self.lb_Folder.SetFocus()
		elif event.GetId() == self.sc_TKesuId:
			self.OnBtnKesu( None )
		elif event.GetId() == self.sc_TKotaeId:
			self.OnButtonKotae( None )
		elif event.GetId() == self.sc_AtarasiiId:
			self.OnButtonAtarasii( None )
		elif event.GetId() == self.sc_HuruiId:
			self.OnButtonHurui( None )
	
	def HakoKaeru( self, index ):
		try:
			self.ImanoHako = self.EncodedHakoNamaeTati[ index ]
			#printself.ImanoHako
			self.imano_page = 0
			typ, data =g_imap4.select( self.ImanoHako, readonly = False)
			#printdata
			self.mail_count = int( data[0] )
			self.tc_Humi.SetValue( str( self.mail_count ) + '個の手紙が有る\n' )
			self.KudariToriyose()
		except:
			g_dm.DasiOku('HakoKaeru: ' + sys.exc_info()[1])
			#print'HakoKaeru:', error_info
			#logging.exception( 'HakoKaeruで過ちが起きた。' )
	
	def HakoItiran( self ):
		self.EncodedHakoNamaeTati = []
		self.lb_Folder.Clear()
		try: #フォルダー達を得る
			re_response = re.compile( br'\((?P<flags>.*?)\) "(?P<delimiter>.*)" (?P<name>.*)' )
			typ, lines = g_imap4.list()
			for line in lines:
				m = re_response.match( line )
				if m:
					flags, delim, name = m.groups()
					if name.startswith( b'"' ) and name.endswith( b'"' ):
						name = name[1:-1]
					folder_name = name.decode( 'imap4-utf-7' )
					#print( '-', folder_name )
					if flags.find( b'\\Noselect' ) == -1:
						self.lb_Folder.Append( folder_name )
						self.EncodedHakoNamaeTati.append( name )
				else:
					pass
					#print( 'Response parse error:', line )
		except:
			wx.MessageBox('箱を得ている間に過ちが起きました。', '過ち', wx.OK | wx.ICON_ERROR )
			if g_debug:
				g_dm.DasiOku('HakoItiran: ' + sys.exc_info()[1])
			#print"フォルダー達を得るで過ちが起きた"
			#logging.exception( 'HakoItiranで過ちが起きた。' )
	
	def KudariToriyose( self ):
		if g_imap4 == None:	return
		
		kudari_ls = []
		try:	#件達を得る
			self.lc_Kudari.DeleteAllItems()
			#printself.mail_count
			if self.mail_count < 1:	return
			tuino_bangou = self.mail_count - SIMESU_KUDARI_KAZU * ( self.imano_page + 1 ) + 1
			if tuino_bangou < 1:
				tuino_bangou = 1
			hazimeno_bangou = self.mail_count - SIMESU_KUDARI_KAZU * self.imano_page
			status, uid_data = g_imap4.fetch( "%d:%d" % ( hazimeno_bangou, tuino_bangou ), '(UID)' )
			for data in uid_data:
				tmp = data.split(b' ')
				#print"tmp[0]:",tmp[0]
				self.UID_dic[ int( tmp[0] ) ] = tmp[2][:-1]
			status, header_data = g_imap4.fetch( "%d:%d" % ( hazimeno_bangou, tuino_bangou ), '(BODY.PEEK[HEADER])' )
			status, flag_data = g_imap4.fetch( '%d:%d' % ( hazimeno_bangou, tuino_bangou ), '(FLAGS)' )
			s_add_ls = []
			#for num in range( SIMESU_KUDARI_KAZU - 1, -1, -1 ):
			for num in reversed( range( SIMESU_KUDARI_KAZU if self.mail_count >= SIMESU_KUDARI_KAZU else self.mail_count ) ):
				sender_add = ''
				#printu'%d番目' % num
				
				res_head = header_data[num * 2][1].decode()
				h_msg = email.message_from_string( res_head )
				decoded_data = email.header.decode_header(h_msg.get('Subject'))
				txt_dec = ""
				for subject, encode in decoded_data:
					if encode != None:
						subject = subject.decode( encode )
					else:
						if not isinstance( subject, str ):
							subject = subject.decode()
					txt_dec = txt_dec + subject
				sender_add = email.header.decode_header(h_msg.get('From'))
				sender_dec = ""
				for atesaki, encode in sender_add:
					if encode != None:
						atesaki = atesaki.decode( encode )
					else:
						if not isinstance( atesaki, str ):
							atesaki = atesaki.decode()
					sender_dec = sender_dec + atesaki
				kudari_ls.append( txt_dec )
				s_add_ls.append( sender_dec )
			index = 0
			item_count = len( kudari_ls )
			for index in range( 0, item_count ):
				self.lc_Kudari.InsertItem( index, '' )
				hata = ""
				#printflag_data[ item_count - 1 - index]
				for flag in imaplib.ParseFlags(flag_data[ item_count - 1 - index]):
					hata = flag.decode() + " " + hata
				if hata == "":	hata = "無し"
				self.lc_Kudari.SetItem( index, 0, hata )
				self.lc_Kudari.SetItem( index, 1, kudari_ls[ index ] )
				self.lc_Kudari.SetItem( index, 2, s_add_ls[ index ] )
		except:
			wx.MessageBox('件を得ている間に過ちが起きました。', '過ち', wx.OK | wx.ICON_ERROR )
			if g_debug:
				g_dm.DasiOku('KudariToriyose: ' + sys.exc_info()[1])
			#logging.exception( 'KudariToriyoseで過ちが起きた。' )
	
	def HumiwoEru( self ):
		try:
			index = self.lc_Kudari.GetFirstSelected( )
			if index == -1:	return
			
			tegami_uid = self.UID_dic[ self.mail_count - index - self.imano_page * SIMESU_KUDARI_KAZU ]
			status, body_data = g_imap4.uid( 'fetch', tegami_uid, '(BODYSTRUCTURE)' )
			status, msg_data = g_imap4.uid( 'fetch', tegami_uid, '(RFC822)' )
			if False:
				pass
			#if msg_data[1][2].find('&Phising') != -1:
			#	self.tc_Humi.SetValue( 'フィッシング（釣り）の疑いがあるので文を映しません' )
			else:
				msg = self.imano_tegami = email.message_from_string( msg_data[0][1].decode() )
				
				#print'After email.message_from_string'
				#printemail.Header.decode_header(msg.get('Subject'))
				#self.encoding = email.Header.decode_header(msg.get('Subject'))[1][1]
				
				self.body = ""
				#file_tati = []
				self.lb_Soe.Clear()
				#添付ファイルを抽出
				self.SoemonoTati = []
				for part in msg.walk():
					#printpart.get_content_type()
					if part.get_content_maintype() == 'multipart':
						continue
					
					filename = part.get_filename()	#ファイル名を取得
					if filename != None:
						g_dm.DasiOku(repr(filename))
						#file_tati.Append(filename)
						namae_dec = ''
						#print(repr(email.header.decode_header(filename)))
						for namae, codec in email.header.decode_header(filename):
							g_dm.DasiOku('namae:%s codec:%s' % (namae, codec))
							if codec != None:
								namae = namae.decode(codec)
							else:
								if not isinstance(namae, str):
									namae = namae.decode()
							namae_dec += namae
						self.lb_Soe.Append(namae_dec)
						self.SoemonoTati.append(part)
					else:	#添え物でなければ
						if part.get_content_type() == 'text/plain':	#更にただの文であればそれをデコード
							char_set = part.get_content_charset()
							if g_debug:
								g_dm.DasiOku(repr(char_set))
							if char_set != None:
								self.tc_Humi.SetValue( part.get_payload( decode=True ).decode( char_set, 'replace' ) )
							else:
								self.tc_Humi.SetValue( part.get_payload( decode = True ) )
							hata = self.lc_Kudari.GetItem( index, 0 ).GetText()
							if hata != '無し' and '\\Seen' not in hata.split( ' ' ):
								hata = hata + ' \\Seen'
							else :
								hata = '\\Seen'
							self.lc_Kudari.SetItem( index, 0, hata )
					
		except:
			wx.MessageBox('数を入れ置いて下さい', '過ち', wx.OK | wx.ICON_ERROR )
			if g_debug:
				g_dm.DasiOku(sys.exc_info()[1])
			#logging.exception( 'HumiwoEruで過ちが起きた。' )
	
	def SoemonoKaku(self, index):
		file_namae = self.lb_Soe.GetString(index)
		if g_debug:
			g_dm.DasiOku(file_namae)
		try:
			fdKaku = wx.FileDialog(self, 'ファイルを書く', '', file_namae, '全てのファイル|*', wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
			if fdKaku.ShowModal() == wx.ID_CANCEL:  return
			file = open(fdKaku.GetPath(), 'wb')
			file.write(self.SoemonoTati[index].get_payload(decode = True))
			file.close()
		except:
			wx.MessageBox('添えられたファイルを書いている時に過ちが起きました。', '過ち', wx.OK | wx.ICON_ERROR )
			if g_debug:
				g_dm.DasiOku(repr(sys.exc_info()))
	
	def OnBtnTBKesu( self, event ):
		#printu"手紙箱を消す"
		index = self.lb_Folder.GetSelection()
		if index != wx.NOT_FOUND and g_imap4 != None:
			g_imap4.delete( self.lb_Folder.GetString( index ).encode( 'imap4-utf-7' ) )
			#printself.lb_Folder.GetString( index ).encode( 'imap4-utf-7' )
			self.HakoItiran()
	
	def OnBtnTBTukuru( self, event ):
		#printu"手紙箱を作る"
		te_dialog = wx.TextEntryDialog( self, "Enter text", "Text enter dialog" )
		if te_dialog.ShowModal() == wx.ID_OK:
			namae = te_dialog.GetValue()
			#print"You entered: %s" % namae
			#printnamae.encode( 'imap4-utf-7' )
			if g_imap4 != None:
				g_imap4.create( namae.encode( 'imap4-utf-7' ) )
				self.HakoItiran()
		te_dialog.Destroy()
	
	def OnButtonKaku( self, event ):
		if self.okuruWaku == None:
			self.okuruWaku = PyOkuru.OkuruWaku( self.parent )
			#okuruIta = OkuruIta( self.okuruWaku )
		self.okuruWaku.Show()
	
	def OnButtonKotae( self, event ):
		index = self.lc_Kudari.GetFirstSelected( )
		if index != -1:
		#if self.imano_tegami != None:
			tegami_uid = self.UID_dic[ self.mail_count - index - self.imano_page * SIMESU_KUDARI_KAZU ]
			status, msg_data = g_imap4.uid( 'fetch', tegami_uid, '(RFC822)' )
			self.imano_tegami = email.message_from_string( msg_data[0][1].decode() )
			if self.okuruWaku == None:	#手紙を書く窓を作る
				self.okuruWaku = PyOkuru.OkuruWaku( self.parent )
			atesaki_field = None	#宛て先を設定
			if self.imano_tegami.get('Precedence') == 'list':
				atesaki_field = 'Reply-To'
				m_id = ''
				if g_debug:
					g_dm.DasiOku( self.imano_tegami.get( 'Message-ID' ) )
				for text, enc in email.header.decode_header( self.imano_tegami.get( 'Message-ID') ):
					if g_debug:
						g_dm.DasiOku( "text:%s enc:%s" % ( text, enc ) )
					if enc != None:
						m_id += text.decode( enc )
					else:
						m_id += text
				self.okuruWaku.m_id = m_id
			else:
				atesaki_field = 'From'
				self.okuruWaku.m_id = None
			text_dec = ""
			if g_debug:
				g_dm.DasiOku( self.imano_tegami.get( atesaki_field ) )
			for text, enc in email.header.decode_header( self.imano_tegami.get( atesaki_field) ):
				if g_debug:
					g_dm.DasiOku( "text:%s enc:%s" % ( text, enc ) )
				if enc != None:
					text_dec += text.decode( enc )
				else:
					if isinstance(text, str):	text_dec += text
					else:	text_dec += text.decode()
			self.okuruWaku.ita.cmb_Atesaki.SetValue( text_dec )
			#文の引用
			if self.cb_Inyou.IsChecked():
				motono_humi = self.tc_Humi.GetValue()
				inyou_humi = ""
				for gyou in motono_humi.split('\n'):
					inyou_humi += "> " + gyou + "\n"
				self.okuruWaku.ita.tc_Humi.SetValue( inyou_humi )
			else:
				self.okuruWaku.ita.tc_Humi.Clear()
			decoded_data = email.header.decode_header( self.imano_tegami.get('Subject') )
			# 件を入れる
			txt_dec = ""
			#MLnoTegami = self.imano_tegami.get('List-Id') != None
			#print( decoded_data )
			for subject, encode in decoded_data:
				if encode != None:
					subject = subject.decode( encode )
				else:
					if not isinstance( subject, str ):
						subject = subject.decode()
				txt_dec = txt_dec + subject
			self.okuruWaku.ita.tc_Kudari.SetValue( 'Re:' + txt_dec )
			# 窓を見せる
			self.okuruWaku.Show()
		else:
			wx.MessageBox('手紙が選ばれていません', '過ち', wx.OK | wx.ICON_ERROR )
	
	def OnBtnKesu( self, event ):
		#print"Kesu"
		index = self.lc_Kudari.GetFirstSelected( )
		#print'index:',index
		if index == -1:	return
		
		#status, data = g_imap4.select( self.ImanoHako, readonly = False )
		sina_kazu = self.lc_Kudari.GetItemCount()
		while index != -1:
			tegami_uid = self.UID_dic[ self.mail_count - index - self.imano_page * SIMESU_KUDARI_KAZU ]
			status, msg_data = g_imap4.uid( 'fetch', tegami_uid, '(RFC822)' )
			msg = email.message_from_string( msg_data[0][1].decode() )
			g_imap4.append( "[Gmail]/&MLQw33ux-", '\\Seen', None, msg.as_bytes() )
			g_imap4.uid('STORE', tegami_uid, '+FLAGS', '(\\Deleted)' )
			index = self.lc_Kudari.GetNextSelected(index)
		self.mail_count = self.mail_count - sina_kazu
		self.KudariToriyose()
	
	def OnButtonAtarasii( self, event ):
		if self.imano_page > 0:
			self.imano_page -= 1
			self.KudariToriyose()
	
	def OnButtonHurui( self, event ):
		if SIMESU_KUDARI_KAZU * self.imano_page < self.mail_count:
			self.imano_page += 1
			self.KudariToriyose()
	
	def OnBtnMoziSettei( self, event ):
		dialog = MoziSetteiDialog( self )
		try:
			dialog.ShowModal()
		finally:
			dialog.Destroy()
	
	def ToziruDe(self):
		file = codecs.open('PyTegami.txt', 'r', 'utf-8')
		fs_kaita = False
		f_nakami = file.read()
		file.close()
		file = codecs.open('PyTegami.txt', 'w', 'utf-8')
		for line in f_nakami.split('\r\n'):
			if line[:9] == 'FONT_OOKISA':
				if line[9:] != str(g_font_ookisa):
					file.write('FONT_OOKISA' + str(g_font_ookisa))
				else:
					file.write(line)
				fs_kaita = True
			elif line == '':
				continue
			else:
				file.write(line)
			file.write('\r\n')	
		if not fs_kaita:
			file.write('FONT_OOKISA' + str(g_font_ookisa) + '\r\n')
		file.close()

class AccountDialog( wx.Dialog ):
	# IDと合い言葉を備えるダイアログ
	def __init__( self, parent ):
		wx.Dialog.__init__( self, parent, title = "アカウントを設定", size = (300, 300) )
		
		st_ID = wx.StaticText( self, label = "ID（名前）" )
		self.tc_ID = wx.TextCtrl( self )
		st_AiKotoba = wx.StaticText( self, label = "合い言葉（パスワード）" )
		self.tc_AiKotoba = wx.TextCtrl( self )
		st_Namae = wx.StaticText(self, label = '送り主として示す名前') 
		self.tc_Namae = wx.TextCtrl(self)
		btn_Settei = wx.Button( self, -1, "設定" )
		btn_Torikesi = wx.Button( self, wx.ID_CANCEL, "取り消し" )
		
		HSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		HSizer1.Add( st_ID, 0, wx.SHAPED )
		HSizer1.Add( self.tc_ID, 1, wx.EXPAND )
		HSizer2 = wx.BoxSizer( wx.HORIZONTAL )
		HSizer2.Add( st_AiKotoba, 0, wx.SHAPED )
		HSizer2.Add( self.tc_AiKotoba, 1, wx.EXPAND )
		HSizer3 = wx.BoxSizer( wx.HORIZONTAL )
		HSizer3.Add( st_Namae, 0, wx.SHAPED )
		HSizer3.Add( self.tc_Namae, 1, wx.EXPAND )
		HSizer4 = wx.BoxSizer( wx.HORIZONTAL )
		HSizer4.Add( btn_Settei )
		HSizer4.Add( btn_Torikesi )
		VSizer = wx.BoxSizer( wx.VERTICAL )
		VSizer.Add( HSizer1, 0, wx.EXPAND )
		VSizer.Add( HSizer2, 0, wx.EXPAND )
		VSizer.Add( HSizer3, 0, wx.EXPAND )
		VSizer.Add(HSizer4, 0, wx.EXPAND)
		
		self.SetSizer( VSizer )
		self.SetAutoLayout(1)
		
		self.Bind( wx.EVT_BUTTON, self.OnBtnSettei, btn_Settei )
		#self.Bind(wx.EVT_CLOSE, self.ToziruDe, self )
	
	def OnBtnSettei( self, event ):
		try:
			f_omo = codecs.open( "PyTegami.txt", 'r+', 'utf-8' )
		except FileNotFoundError:
			f_omo = codecs.open( "PyTegami.txt", 'w+', 'utf-8' )
		try:
			if g_debug:
				g_dm.DasiOku('Account Dialog, OnBtnSettei')
			list_nakami = f_omo.read().split('\r\n')
			f_omo.seek(0,0)
			f_omo.write( 'ID' + self.tc_ID.GetValue() + "\r\n" )
			f_omo.write( 'PASSWORD' + self.tc_AiKotoba.GetValue() + '\r\n' )
			f_omo.write( 'NAMAE' + self.tc_Namae.GetValue() + '\r\n' )
			if len(list_nakami) >= 4:
				for i in range(3,len(list_nakami)):
					if g_debug:
						g_dm.DasiOku('index:' + str(i))
					f_omo.write(list_nakami[i] + '\r\n')
			self.EndModal( wx.ID_OK )
		except:
			wx.MessageBox('アカウント情報をファイルに書いている間に過ちが起きました。', '過ち', wx.OK | wx.ICON_ERROR )
			if g_debug:
				g_dm.DasiOku(repr(sys.exc_info()))

class OmonaWaku(wx.Frame):
	def __init__( self, parent, title = "Py手紙" ):
		screen_x = ctypes.windll.user32.GetSystemMetrics( 0 )
		screen_y = ctypes.windll.user32.GetSystemMetrics( 1 )
		wx.Frame.__init__( self, parent, title = title, size = ( screen_x / 100 * 80, screen_y / 100 * 92 ) )
		
		self.CreateStatusBar()
		filemenu = wx.Menu()
		#menuOpen = filemenu.Append(wx.ID_OPEN, "&Open", "Open a file")
		sina_mozi = filemenu.Append( -1, '文字の設定(&M)', '文字の大きさを変える' )
		sina_account = filemenu.Append( -1, 'アカウント設定(&A)', 'アカウント情報を変える' )
		menuTuite = filemenu.Append(wx.ID_ABOUT, "ついて（&T）", " このソフトについて")
		menuExit = filemenu.Append(wx.ID_EXIT, "抜ける(&N)", "このソフトを止める")
		
		self.Bind(wx.EVT_CLOSE, self.ToziruDe, self )
		self.Bind(wx.EVT_MENU, self.TuiteDe, menuTuite )
		self.Bind(wx.EVT_MENU, lambda : self.Close(), menuExit)
		self.Bind( wx.EVT_MENU, self.MoziKaeruDe, sina_mozi )
		self.Bind(wx.EVT_MENU, self.AccountSetteiDe, sina_account)
		
		menuBar = wx.MenuBar()
		menuBar.Append(filemenu, "品書き(&S)")
		self.SetMenuBar(menuBar)
		
		self.ita = OmonaIta( self )
		
	def ToziruDe( self, event ):
		self.ita.ToziruDe()
		try:
			if g_imap4 != None:	g_imap4.logout()
		except:
			pass
		self.Destroy()
	
	def TuiteDe( self, e ):
		wx.MessageBox("電子手紙ソフトウェアー", "Py手紙に付いて", wx.OK)
	
	def MoziKaeruDe( self, moyoosi ):
		self.ita.OnBtnMoziSettei( None )
	
	def AccountSetteiDe( self, moyoosi ):
		ad = AccountDialog( None )
		ok_osita = False
		if ad.ShowModal() != wx.ID_OK:
			if g_debug:
				g_dm.DasiOku('アカウント設定ダイアログで設定を押さなかった')
		else:
			ok_osita = True
		ad.Destroy()
		if ok_osita:
			ServerTunagu()
			self.ita.HakoItiran()
			self.ita.HakoKaeru(0)

if __name__ == '__main__':
	app = wx.App(False)
	account_henshuu = False
	if len( sys.argv ) >= 2:
		for argv in sys.argv:
			if argv.lower() == '-t':
				g_tunagu = False
			elif argv.lower() == '-d':
				g_debug = True
			elif argv.lower() == '-r':
				FORMAT = '%(asctime)-15s %(message)s'
				logging.basicConfig( format = FORMAT, filename = '過ちのログ.txt', level = logging.DEBUG )
				logging.info( 'ログを取ります。' )
			elif argv.lower() == '-a':
				account_henshuu = True
	if g_debug:	#デバッグ情報のための窓
		g_dm = DebugMado( None )
		g_dm.Show()
	if not os.path.exists( 'PyTegami.txt' ) or account_henshuu:
		ad = AccountDialog( None )
		if ad.ShowModal() != wx.ID_OK:
			if g_debug:
				#print('No ok press')
				g_dm.DasiOku('アカウント設定ダイアログで設定を押さなかった')
			sys.exit()
		ad.Destroy()
	if g_tunagu:
		ServerTunagu()
	ow = OmonaWaku( None )
	ow.Show()
	app.MainLoop()
