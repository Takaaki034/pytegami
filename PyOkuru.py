# coding:UTF-8
'''電子手紙ソフトウェアーの送る窓'''

import wx, smtplib, email, sys, codecs
from PyMisc import read_NAMEandPASSWORD, AtesakiChou, namae_yomu, tegami_namae_kaesu
#from email.MIMEText import MIMEText
from email.mime.text import MIMEText
#from email.Header import Header
from email import header
from PyTegami import g_debug
from PyTegami import g_dm

g_tukaiteNamae = None

class GMailSender:
	def __init__( self, encoding, subject, body, to_addr ):
		global g_tukaiteNamae
		self.date = email.utils.formatdate()
		self.encoding = encoding
		self.subject = subject
		self.body = body.encode(encoding)
		self.to_addr = to_addr
		
		g_tukaiteNamae,self.password = read_NAMEandPASSWORD()
		self.from_addr = g_tukaiteNamae + '@gmail.com'
	
	def sendMail( self, message_ID ):
		mail = email.mime.text.MIMEText( self.body, 'plain', self.encoding )
		mail['Subject'] = email.header.Header( self.subject, self.encoding )
		tegami_namae = tegami_namae_kaesu()
		mail['From'] = email.header.Header(tegami_namae + '<' + self.from_addr + '>')
		mail['To'] = email.header.Header(self.to_addr + '<' + self.from_addr + '>')
		mail['Date'] = self.date
		if message_ID != None:
			mail['In-Reply-To'] = message_ID
			mail['References'] = message_ID
		
		o_smtp = smtplib.SMTP( 'smtp.gmail.com', 587)
		o_smtp.ehlo()
		try:
			o_smtp.starttls()
		except:
			o_smtp.close()
			if g_debug:
				g_dm.DasiOku( 'starttls()で例外が起きました' )
			return
		o_smtp.ehlo()
		o_smtp.login( g_tukaiteNamae, self.password )
		o_smtp.sendmail( self.from_addr, self.to_addr, mail.as_string() )
		o_smtp.close()

class OkuruIta( wx.Panel ):
	# 送る手紙を書くための板
	def __init__( self, parent):
		wx.Panel.__init__( self, parent )
		
		g_tukaiteNamae = namae_yomu()
		self.tc_Humi = wx.TextCtrl( self, -1, "", style=wx.TE_MULTILINE )
		btn_Okuru = wx.Button( self, -1, "手紙を送る" )
		btn_Sitagaki = wx.Button( self, -1, "下書きとして残す" )
		btn_Kuwaeru = wx.Button( self, -1, "宛て先帳に加える" )
		btn_Nozoku = wx.Button( self, -1, "宛て先帳から除く" )
		#self.tc_Atesaki = wx.TextCtrl( self, -1, "" )
		self.AteChou = AtesakiChou()
		self.cmb_Atesaki = wx.ComboBox( self, choices = self.AteChou.Subete() )
		self.tc_Kudari = wx.TextCtrl( self, -1, "" )
		lbl_Atesaki = wx.StaticText( self, label = "宛て先" )
		lbl_Kudari = wx.StaticText( self, label = "件" )
		
		self.sc_HumiId = wx.NewId()
		self.sc_AtesakiId = wx.NewId()
		self.sc_KudariId = wx.NewId()
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_HumiId )
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_AtesakiId )
		self.Bind( wx.EVT_MENU, self.OnKeyCombo, id = self.sc_KudariId )
		accel_tbl = wx.AcceleratorTable( [ ( wx.ACCEL_ALT, ord('H'), self.sc_HumiId ), 
			( wx.ACCEL_ALT, ord( 'A' ), self.sc_AtesakiId ), ( wx.ACCEL_ALT, ord( 'K' ), self.sc_KudariId ) ] )
		self.SetAcceleratorTable( accel_tbl )
		
		self.Bind( wx.EVT_BUTTON, self.OnButtonOkuru, btn_Okuru )
		self.Bind( wx.EVT_BUTTON, self.OnButtonKuwaeru, btn_Kuwaeru )
		self.Bind( wx.EVT_BUTTON, self.OnButtonNozoku, btn_Nozoku )
		self.Bind( wx.EVT_BUTTON, self.OnBtnSitagaki, btn_Sitagaki )
		
		self.hSizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.hSizer.Add( lbl_Atesaki, 0, wx.SHAPED )
		self.hSizer.Add( self.cmb_Atesaki, 1, wx.EXPAND )
		self.hSizer.Add( btn_Okuru, 0, wx.SHAPED )
		self.hSizer.Add( btn_Sitagaki, 0, wx.SHAPED )
		self.hSizer.Add( btn_Kuwaeru, 0, wx.SHAPED )
		self.hSizer.Add( btn_Nozoku, 0, wx.SHAPED )
		
		hSizer2 = wx.BoxSizer( wx.HORIZONTAL )
		hSizer2.Add( lbl_Kudari, 0, wx.SHAPED )
		hSizer2.Add( self.tc_Kudari, 1, wx.EXPAND )
		
		self.vSizer = wx.BoxSizer(wx.VERTICAL)
		self.vSizer.Add( self.hSizer, 0, wx.EXPAND )
		self.vSizer.Add( hSizer2, 0, wx.EXPAND )
		self.vSizer.Add( self.tc_Humi, 1, wx.EXPAND )
		
		self.SetSizer(self.vSizer)
		self.SetAutoLayout(1)
		self.Sizer.Fit(self)
		self.m_id = None
		
		self.parent = parent
		
	def OnKeyCombo( self, event ):
		eventId = event.GetId()
		if eventId == self.sc_HumiId:
			self.tc_Humi.SetFocus()
		elif eventId == self.sc_AtesakiId:
			self.cmb_Atesaki.SetFocus()
		elif eventId == self.sc_KudariId:
			self.tc_Kudari.SetFocus()
	
	def OnBtnSitagaki( self, event ):
		#print 'Sitagaki call'
		#print g_tukaiteNamae
		self.parent.parent.Sitagaki( self.tc_Humi.GetValue(), self.tc_Kudari.GetValue(), g_tukaiteNamae + '@gmail.com', self.cmb_Atesaki.GetValue() )
	
	def OnButtonNozoku( self, event ):
		atesaki = self.cmb_Atesaki.GetValue()
		#print "宛て先帳から除く", atesaki
		self.AteChou.Nozoku( atesaki )
		eranda_sirube = self.cmb_Atesaki.FindString( atesaki )
		if eranda_sirube != wx.NOT_FOUND:
			self.cmb_Atesaki.Delete( eranda_sirube )
	
	def OnButtonKuwaeru( self, event ):
		atesaki = self.cmb_Atesaki.GetValue()
		#print "宛て先帳に加える", atesaki
		self.AteChou.Kuwaeru( atesaki )
		self.cmb_Atesaki.Append( atesaki )
	
	def OnButtonOkuru( self, event):
		#print "Send a mail"
		kudari = self.tc_Kudari.GetValue()
		if kudari != '':
			if g_debug:
				g_dm.DasiOku('PyOkuru, cmb_Atesaki:' + self.cmb_Atesaki.GetValue() )
			gm_s = GMailSender( 'iso-2022-jp', kudari, self.tc_Humi.GetValue(), self.cmb_Atesaki.GetValue() )
			gm_s.sendMail(self.m_id)
			self.parent.Hide()
	
	def OnClose( self ):
		#print 'OnClose()'
		self.AteChou.Kaku()

class OkuruWaku(wx.Frame):
	#手紙を送るための枠
	def __init__( self, parent, title = "手紙を送る", size = (800, 500) ):
		wx.Frame.__init__( self, parent, title = title, size = size )
		
		self.CreateStatusBar()
		filemenu = wx.Menu()
		menuExit = filemenu.Append(wx.ID_EXIT, "閉じる(&T)", "この窓を閉じる")
		
		menuBar = wx.MenuBar()
		menuBar.Append(filemenu, "ファイル（&F）")
		self.SetMenuBar(menuBar)
		self.Bind(wx.EVT_CLOSE, self.OnClose, self )
		
		self.ita = OkuruIta( self )
		self.parent = parent
	
	def OnClose(self, event):
		if event.CanVeto():
			self.ita.OnClose()
			self.Hide()
		else:
			self.Destroy()
